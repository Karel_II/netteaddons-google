<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\Bridges\GoogleServicesDI;

use Nette;
use Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\DI\Statement;
use \NetteAddons\GoogleServices\Application\UI\IAnalyticsControlFactory,
    \NetteAddons\GoogleServices\Application\UI\AnalyticsControlFactory;
use \NetteAddons\GoogleServices\Presenters\SearchConsolePresenter;
use NetteAddons\GoogleServices\Routers\RouteUpdater;
use \NetteAddons\GoogleServices\GoogleService;

/**
 * Description of GoogleSearchConsoleExtension
 *
 * @author karel.novak
 */
final class GoogleServicesExtension extends Nette\DI\CompilerExtension {

    const GOOGLE_SEARCH_CONSOLE_MODULE = 'GoogleServices';

    private $googleServicesDefaults = [
        'enable' => NULL,
        'searchConsoleCode' => null,
        'analyticsServiceId' => 'UA-XXXXX-Y',
    ];

    /**
     * 
     * @return void
     */
    public function loadConfiguration() {
        $this->validateConfig($this->googleServicesDefaults);
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('googleService'))
                ->setFactory(GoogleService::class)
                ->addSetup('setEnable', [$this->config['enable']])
                ->addSetup('setAnalyticsServiceId', [$this->config['analyticsServiceId']])
                ->addSetup('setSearchConsoleCode', [$this->config['searchConsoleCode']]);

        $builder->addDefinition($this->prefix('googleAnalyticsControlFactory'))
                ->setImplement(IAnalyticsControlFactory::class);

        if ($this->config['searchConsoleCode'] !== NULL) {

            /**
             * Add Route
             */
            $detailsRouter = new RouteList(self::GOOGLE_SEARCH_CONSOLE_MODULE);
            $detailsRouter[] = new Route('/' . $this->config['searchConsoleCode'], self::GOOGLE_SEARCH_CONSOLE_MODULE . ':SearchConsole:verification');
            $builder->addDefinition($this->prefix("routeUpdater"))->
                    setFactory(RouteUpdater::class, ['routeList' => $detailsRouter]);
            $builder->getDefinition($builder->getByType(\Nette\Application\Application::class))->
                    addSetup('$onStartup[]', [new Statement($this->prefix('@routeUpdater::init'))]);

            /*
             * Add Mappings
             */
            $presenterFactory = $builder->getDefinition($builder->getByType(\Nette\Application\IPresenterFactory::class));
            $presenterFactory->addSetup('setMapping', [[self::GOOGLE_SEARCH_CONSOLE_MODULE => ['NetteAddons', '*', 'Presenters\*Presenter']]]);
        }
    }

}
