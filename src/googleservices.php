<?php

spl_autoload_register(function ($className) {
    static $classMapBridge = [
        /**
         * NetteAddons\Bridges\GoogleServicesDI
         */
        'NetteAddons\\Bridges\\GoogleServicesDI\\GoogleServicesExtension' => 'GoogleServicesDI/GoogleServicesExtension.php',
    ];

    static $classMapGoogleServices = [
        /**
         * NetteAddons\GoogleServices\Application\UI
         */
        'NetteAddons\\GoogleServices\\Application\\UI\\AnalyticsControl' => 'Application/UI/AnalyticsControl.php',
        'NetteAddons\\GoogleServices\\Application\\UI\\IAnalyticsControlFactory' => 'Application/UI/IAnalyticsControlFactory.php',
        /**
         * NetteAddons\GoogleServices\Presenters
         */
        'NetteAddons\\GoogleServices\\Presenters\\SearchConsolePresenter' => 'Presenters/SearchConsolePresenter.php',
        /**
         * NetteAddons\GoogleServices\Routers
         */
        'NetteAddons\\GoogleServices\\Routers\\RouteUpdater' => 'Routers/RouteUpdater.php',
        /**
         * NetteAddons\GoogleServices\GoogleService
         */
        'NetteAddons\\GoogleServices\\GoogleService' => 'GoogleService.php',
    ];
    if (isset($classMapBridge[$className])) {
        require __DIR__ . '/Bridges/' . $classMapBridge[$className];
    }
    if (isset($classMapGoogleServices[$className])) {
        require __DIR__ . '/GoogleServices/' . $classMapGoogleServices[$className];
    }
});
