<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\GoogleServices;

use Nette;
/**
 * Description of GoogleService
 *
 * @author Karel
 * 
 * @property type $enable Description
 * @property type $searchConsoleCode Description
 * @property type $analyticsServiceId Description
 * 
 */
class GoogleService {
    
    use Nette\SmartObject;

    const ANALYTICS_PERMISSION_RESOURCE = 'googleAnalytics';

    /**
     *
     * @var bool 
     */
    private $enable = FALSE;

    /**
     *
     * @var string 
     */
    private $searchConsoleCode = NULL;

    /**
     * UA-XXXXX-Y
     * @var string 
     */
    private $analyticsServiceId = NULL;

    /**
     *
     * @var \Nette\Security\Permission 
     */
    private $permission = NULL;

    /**
     *
     * @var \Nette\Security\User 
     */
    private $user = NULL;

    /**
     * 
     * @param \Nette\Security\Permission $permission
     * @param \Nette\Security\User $user
     */
    public function __construct(\Nette\Security\Permission $permission, \Nette\Security\User $user) {
        $this->permission = $permission;
        $this->user = $user;
        if (!$this->permission->hasResource(self::ANALYTICS_PERMISSION_RESOURCE)) {
            $this->permission->addResource(self::ANALYTICS_PERMISSION_RESOURCE);
        }
    }

    /**
     * 
     * @return type
     */
    public function getEnable() {
        if ($this->enable === TRUE | $this->enable === FALSE) {
            return $this->enable;
        }
        return $this->user->isAllowed(self::ANALYTICS_PERMISSION_RESOURCE);
    }

    /**
     * 
     * @return type
     */
    function getSearchConsoleCode() {
        return $this->searchConsoleCode;
    }

    /**
     * 
     * @return type
     */
    function getAnalyticsServiceId() {
        return $this->analyticsServiceId;
    }

    /**
     * 
     * @param type $enable
     */
    function setEnable($enable) {
        $this->enable = $enable;
    }

    /**
     * 
     * @param type $searchConsoleCode
     */
    function setSearchConsoleCode($searchConsoleCode) {
        $this->searchConsoleCode = $searchConsoleCode;
    }

    /**
     * 
     * @param type $analyticsServiceId
     */
    function setAnalyticsServiceId($analyticsServiceId) {
        $this->analyticsServiceId = $analyticsServiceId;
    }

}
