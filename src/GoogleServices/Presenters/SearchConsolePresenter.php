<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\GoogleServices\Presenters;

use Nette;

/**
 * Description of SearchConsolePresenter
 *
 * @author Karel
 */
class SearchConsolePresenter extends Nette\Application\UI\Presenter {

    /**
     * @var \NetteAddons\GoogleServices\GoogleService
     * @inject
     */
    public $googleService = NULL;

    public function renderVerification() {
        $this->template->googleService = $this->googleService;
    }

}
