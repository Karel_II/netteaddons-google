<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\GoogleServices\Application\UI;

/**
 *
 * @author Karel
 */
interface IAnalyticsControlFactory {

    /**
     * 
     * @return \NetteAddons\GoogleServices\Application\UI\AnalyticsControl
     */
    public function create();
}
