<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\GoogleServices\Application\UI;

use NetteAddons\GoogleServices\GoogleService;

/**
 * Description of GoogleAnalyticsControl
 *
 * @author Karel
 */
class AnalyticsControl extends \Nette\Application\UI\Control {

    /**
     *
     * @var string 
     */
    protected $view = 'default';
    /*
     * 
     * @var bool     
     */
    protected $resetTemplateFile = false;

    /**
     * Formats view template file names.
     * @return array
     */
    public function formatTemplateFiles() {
        $dir = dirname($this->getReflection()->getFileName());
        $dir = is_dir("$dir/templates") ? $dir : dirname($dir);
        return array(
            "$dir/templates/$this->view.latte",
            "$dir/templates/$this->view.phtml",
        );
    }

    public function getTemplate() {
        $template = parent::getTemplate();
        if (!$template) {
            return;
        }

        if (!$template->getFile() || $this->resetTemplateFile) { // content template
            $files = $this->formatTemplateFiles();
            foreach ($files as $file) {
                if (is_file($file)) {
                    $template->setFile($file);
                    break;
                }
            }

            if (!$template->getFile()) {
                $file = preg_replace('#^.*([/\\\\].{1,70})\z#U', "\xE2\x80\xA6\$1", reset($files));
                $file = strtr($file, '/', DIRECTORY_SEPARATOR);
                throw new \Exception("Page not found. Missing template '$file'.");
            }
            $this->resetTemplateFile = false;
        }
        return $template;
    }

    /**
     * @var \NetteAddons\GoogleServices\GoogleService
     */
    private $googleService = NULL;

    public function __construct(GoogleService $googleService) {
        $this->googleService = $googleService;
        parent::__construct();
    }

    public function render() {
        $this->template->googleService = $this->googleService;
        $this->template->render();
    }

}
