<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace NetteAddons\GoogleServices\Routers;

use Nette;

/**
 * Description of RouteUpdater
 *
 * @author Karel
 */
final class RouteUpdater {

    use \Nette\SmartObject;

    /**
     * @var \Nette\Application\IRouter
     */
    protected $router = NULL;

    /**
     * @var Nette\Application\Routers\RouteList 
     */
    protected $routeList = NULL;

    function __construct(\Nette\Application\IRouter $router, Nette\Application\Routers\RouteList $routeList) {
        $this->router = $router;
        $this->routeList = $routeList;
    }

    public function addRoute() {
        $this->router->prepend($this->routeList);
    }

    public function init() {
        return [$this, 'addRoute'];
    }

}
